<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>

</head>

<?php /* Custum theme style*/ ?>
<body class="<?php print $body_classes; ?>">
<div id="topwrapper"></div>
	<div id="wrapper">
<!---- navigation -------->	 
<div id="navpanel"  class="menu <?php if ($primary_links) { print "withprimary"; } if ($secondary_links) { print " withsecondary"; } ?> ">
    <?php if ($primary_links): ?>
    <div id="primary" class="clear-block"> <?php print theme('menu_links', $primary_links); ?> </div>
    <?php endif; ?>
    <?php if ($secondary_links): ?>
    <div id="secondary" class="clear-block"> <?php print theme('menu_links', $secondary_links); ?> </div>
    <?php endif; ?>
</div>
<!-- /navigation -->
<!-- header -->
<div id="header">
			<?php if ($logo): ?>
				<a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>"> <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo" /> </a>
			<?php endif; ?>
<div id="title">
				<?php if ($site_name): ?>
					<h1 id='home'> <a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>"> <?php print $site_name; ?> </a> </h1>
				<?php endif; ?>
				
			</div>		
  </div>
<!-- /header -->		
		<!-- main content -->
	
		<div id="containermiddle">
		    <div id="maincontainer" class="clear-block">
				<!-- .......... sidebar-left .......... -->
				
					<?php if ($sidebar_left): ?>
						<div id="sidebar-left"> <?php print $sidebar_left; ?> </div>
					<?php endif; ?>
				
				<!-- .......... /sidebar-left .......... -->
				<!-- .......... main .......... -->
				<div id="main">
						<?php if ($mission): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
						<?php if ($title): ?><h1 class="title"><?php print $title; ?></h1><?php endif; ?>
						<?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
						<?php print $help; ?>
						<?php print $messages; ?>
						<?php print $content; ?>
				</div>
				<!-- .......... /main content.......... -->
				<!-- .......... sidebar-right .......... -->
				<?php if ($sidebar_right): ?>
				<div id="sidebar-right"> <?php print $sidebar_right; ?> </div>
				<?php endif; ?>
				<!-- .......... /sidebar-right .......... -->
			</div>
		</div>
		<div id="footer">
				<p>Copyright infomation comes here Theme by <a href="http://drupal.org/user/198692">Rishikesh</a></p>
				<p><?php print $footer_message; ?></p>
		</div>
	</div><div id="bottomwrapper"></div>
</body>
</html>
